# KeplerCalc-cpp

## Description
Small project that explores implementations for the Kepler problem, determining the position of a satellite based on position and speed at a previous known time.
Written in C++20.
At the moment, the focus lies on benchmarking the performance of different implementations of solutions to the Kepler problem.

## Installation/Compilation
### Requirements
- CMake (Version ?)
- Eigen (Recommended Version 3.4.0) (Homepage)[eigen.tuxfamily.org]
- Google Benchmark (Github Repository)[https://github.com/google/benchmark]
- C++ Compiler capable of compiling C++20 (I use Clang 13/14 or GCC 10 and later)


## Usage
Adjust the main.cpp to your liking; i.e. change what methods are benchmarked in which way. Then build the project (I recommend release mode for benchmarking) and run it. Depending on your main.cpp setup, different methods for calculating r and v from an ephemeris should then be benchmarked.

### Example output:
```
/keplercalc-cpp/bin/keplercalc 
2022-03-22T10:10:07+01:00
Running /home/bernhard/Projects/keplercalc-cpp/bin/keplercalc
Run on (24 X 4341.28 MHz CPU s)
CPU Caches:
  L1 Data 32 KiB (x12)
  L1 Instruction 32 KiB (x12)
  L2 Unified 512 KiB (x12)
  L3 Unified 32768 KiB (x2)
Load Average: 0.61, 0.58, 0.54
----------------------------------------------------------------
Benchmark                      Time             CPU   Iterations
----------------------------------------------------------------
BM_r0v0_custom              80.6 ns         80.6 ns      8622224
BM_r0v0_custom_legacy        134 ns          134 ns      5199115
```
This is running on an AMD Ryzen R9 5900X.

## Support
Please open a new issue and describe your issue in detail. This is more of an experimental project, so your issue may or may not be fixed.

## Roadmap
1. Usable via CLI (2022)
2. Usable via CLI with usability features (2022/2023)
3. Graphical User Interface (2024++)

## Contributing
In general, I do not expect contributions for this small project. This may change in the future. For now, contributions for specific issues will be discussed individually.

## Author(s) and acknowledgment
Current Author and Maintainer: Bernhard Luedtke

David Vallado's implementation of math/calc functions related to the Kepler problem (partially included in /include/vallado) is included in this repository. The original code has been retrieved from https://celestrak.com/software/vallado-sw.php (C++ variant). I've exchanged a few mails with Mr. Vallado and confirmed that acknowledgment and link-to-the-source is sufficient attribution for use in an open source project like this. If you have any questions regarding this code, please ask me first, maybe my changes are the cause of your question.

## License
This project is licensed under the Apache License, Version 2.0 (URL http://www.apache.org/licenses/LICENSE-2.0)
