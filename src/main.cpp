/**
 * @file main.cpp
 * @author Bernhard Luedtke
 * @brief 
 * @version 0.1
 * @date 2022-01-27
 * 
 * @copyright Copyright 2022 Bernhard Luedtke

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#include "common/constants.hpp"
#include "impl_eigen/keplercalc.hpp"
#include "common/ephemeris.hpp"

#include <cmath>
#include <cstddef>
#include <ios>
#include <iostream>
#include <iomanip>
#include <math.h>
#include <numbers>
#include <chrono>
#include <thread>

#include <benchmark/benchmark.h>
#include <vector>
#include <random>

using namespace bluedtke;
using std::cout;
using std::get; //NOLINT
using namespace std::chrono;
using namespace std::chrono_literals;
constexpr size_t targetSize = 100000;

static std::vector<orb_elements_ext> getOrbsRDMVec(size_t sizeTarget){
   using std::numbers::pi;
   //setup
   //Random Static seed for reproducability
   std::mt19937_64 engine(7126307);
   std::uniform_real_distribution<> anglesDegFullDistribution(0.0, 359.99);
   std::uniform_real_distribution<> semiMajorAxisDistribution(13000.0, 300000.0);
   std::uniform_real_distribution<> eccentDistribution(0.0, 0.99);
   
   std::vector<orb_elements_ext> orbExt_rdm {};
   orbExt_rdm.reserve(sizeTarget);
   
   for (size_t i = 0; i < sizeTarget; ++i) {
      double a = semiMajorAxisDistribution(engine);
      double T = ((2*pi) / sqMU_const) * std::pow(a, 3.0/2.0);
      orbExt_rdm.push_back(calculateExtendedEphemeris( {
            a, // a
            eccentDistribution(engine), //e
            degToRad(anglesDegFullDistribution(engine)*0.5), // i
            degToRad(anglesDegFullDistribution(engine)), // Omega
            degToRad(anglesDegFullDistribution(engine)), // w
            degToRad(anglesDegFullDistribution(engine)), // _v_
            T 
            }));
   }
   return orbExt_rdm;
}

static void BM_r0v0_custom(benchmark::State& state) {
   const auto orbExt_rdm = getOrbsRDMVec(targetSize);
   //Benchmark
   size_t i = 0;
   for (auto _ : state) {
      r0_v0 res;
      benchmark::DoNotOptimize(res = get_r0v0_from_elements( orbExt_rdm.at(i) ));
      benchmark::ClobberMemory();
      i = (i < targetSize-1) ? i+1 : 0;
   }
}

static void BM_r0v0_custom_experiments(benchmark::State& state) {
   const auto orbExt_rdm = getOrbsRDMVec(targetSize);
   //Benchmark
   size_t i = 0;
   for (auto _ : state) {
      r0_v0 res;
      benchmark::DoNotOptimize(res = get_r0v0_from_elements_tests( orbExt_rdm.at(i) ));
      benchmark::ClobberMemory();
      i = (i < targetSize-1) ? i+1 : 0;
   }
}

static void BM_r0v0_custom_legacy(benchmark::State& state) {
   const auto orbExt_rdm = getOrbsRDMVec(targetSize);
   //Benchmark
   size_t i = 0;
   for (auto _ : state) {
      r0_v0 res;
      benchmark::DoNotOptimize(res = get_r0v0_from_elements_legacy( orbExt_rdm.at(i) ));
      benchmark::ClobberMemory();
      i = (i < targetSize-1) ? i+1 : 0;
   }
}

static void BM_r0v0_vallado(benchmark::State& state) {
   auto orbExt_rdm = getOrbsRDMVec(targetSize);
   //Benchmark
   size_t i = 0;
   for (auto _ : state) {
      r0_v0 res;
      benchmark::DoNotOptimize(res = callVallado_coe2rv_ext( orbExt_rdm.at(i) ));
      benchmark::ClobberMemory();
      i = (i < targetSize-1) ? i+1 : 0;
   }
}

static void BM_r0v0_vallado_no_return(benchmark::State& state) {
   auto orbExt_rdm = getOrbsRDMVec(targetSize);
   //Benchmark
   size_t i = 0;
   for (auto _ : state) {
      callVallado_coe2rv_ext_no_ret( orbExt_rdm.at(i) );
      benchmark::ClobberMemory();
      i = (i < targetSize-1) ? i+1 : 0;
   }
}

BENCHMARK(BM_r0v0_custom_legacy);
BENCHMARK(BM_r0v0_custom);
BENCHMARK(BM_r0v0_custom_experiments);
BENCHMARK(BM_r0v0_vallado);
BENCHMARK(BM_r0v0_vallado_no_return);



BENCHMARK_MAIN();







//This main is renamed to main_ when BENCHMARK_MAIN() is supposed to be run only.
int main_(int argc, char **argv){
   cout << "Start of KeplerCalc\n" << std::fixed;
   
   auto orbsExt = calculateExtendedEphemeris({42174.0,  //a
                     0.0002470,  //e
                     degToRad(9.5762),   //i
                     degToRad(303.3260),    //Om, laasc todo convert to rad
                     degToRad(335.8957),   //w, argper, todo convert to rad
                     degToRad(0.0),    //nu TODO: Change to mean // 221.66778052607748
                     86193.56800054129 });
   
   auto orbsExt2 = calculateExtendedEphemeris({22174.0,  //a
                     0.01,  //e
                     degToRad(5.5),   //i
                     degToRad(60.3),    //Om, laasc todo convert to rad
                     degToRad(75.2),   //w, argper, todo convert to rad
                     degToRad(50.0),    //nu TODO: Change to mean // 221.66778052607748
                     66193.0 });
   
   auto& orbsToTest = orbsExt2;
   
   cout << "New Method: \n";
   auto r0v0 = get_r0v0_from_elements(orbsToTest);
   
   cout << "r:\n";
   for(auto i = 0; i < 3; ++i){
      cout << "   " << r0v0.r0[i] << "\n";
   }
   cout << "v:\n";
   for(auto i = 0; i < 3; ++i){
      cout << "   " << r0v0.v0[i] << "\n";
   }
   
   cout << "\nOld Method: \n";
   r0v0 = get_r0v0_from_elements_legacy(orbsToTest);
   
   cout << "r:\n";
   for(auto i = 0; i < 3; ++i){
      cout << "   " << r0v0.r0[i] << "\n";
   }
   cout << "v:\n";
   for(auto i = 0; i < 3; ++i){
      cout << "   " << r0v0.v0[i] << "\n";
   }
   
   //===========================================================================================
   /*
   double per = ((2*std::numbers::pi)/sqMU_const) * std::pow(42174.0, 3.0/2.0);
   //cout << "Test: " << per << "\n";
   KeplerCalc calcObj{};
   
   orb_elements orbs  = {42174.0,  //a
                     0.0002470,  //e
                     degToRad(9.5762),   //i
                     degToRad(303.3260),    //Om, laasc todo convert to rad
                     degToRad(335.8957),   //w, argper, todo convert to rad
                     degToRad(221.66778052607748),    //nu TODO: Change to mean -> calc
                     86193.5680005413     //T, seconds
                     };
   auto orbsExt = calculateExtendedEphemeris(orbs);
   
   auto start1 = steady_clock::now();
   auto r0v0_2 = callVallado_coe2rv_ext(orbsExt);
   //auto r0v0_2 = get_r0v0_from_elements(orbsExt);
   auto end1 = steady_clock::now();
   std::cout << "B TIME: " << duration_cast<nanoseconds>(end1 - start1).count() << " ns\n";
   */ // NOLINT
   
   
   /*
   cout << "ADAPTED METHOD:\n";
   cout << "r:\n";
   for(auto i = 0; i < 3; ++i){
      cout << "   " << r0v0.r0[i] << "\n";
   }
   cout << "v:\n";
   for(auto i = 0; i < 3; ++i){
      cout << "   " << r0v0.v0[i] << "\n";
   }*/
   /*
   std::this_thread::sleep_for(100ms);
   auto start0 = steady_clock::now();
   auto r0v0 = get_r0v0_from_elements_shortEph(orbs);
   auto end0 = steady_clock::now();
   std::cout << "A TIME: " << duration_cast<nanoseconds>(end0 - start0).count() << " ns\n";
   */
   cout << "End of KeplerCalc. Goodbye.\n";
   return 0;
}