/**
 * @file keplercalc.hpp
 * @author Bernhard Luedtke
 * @brief 
 * @version 0.1
 * @date 2022-01-27
 * 
 * @copyright Copyright 2022 Bernhard Luedtke

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
 
#ifndef KEPLERCALC_HPP
#define KEPLERCALC_HPP

#include <tuple>
#include "../common/constants.hpp"
#include "../common/ephemeris.hpp"
 
#include <eigen3/Eigen/Core>
 
namespace bluedtke {
   
class KeplerCalc {
public:
   
   r0_v0 calculate_positon_and_speed(double t_now);
   
   //TODO annotate units
   void set_semiMajorAxis(double _semiMajorAxis) noexcept{
      semiMajorAxis = _semiMajorAxis;
   }
   void set_r0(double r0_0, double r0_1, double r0_2){
      r_0 = {r0_0, r0_1, r0_2};
   }
   void set_v0(double v0_0, double v0_1, double v0_2){
      v_0 = {v0_0, v0_1, v0_2};
   }
   void set_t0(double _t_0){
      t_0 = _t_0;
   }
   
private:
    //Need the following data:
   // Semi Major Axis
   // r0, v0, t0
   
   double semiMajorAxis = 10000.0;
   volatile double r_0_length = 1.0;
   Eigen::Vector3d r_0 {1.0, 0.0, 0.0};
   Eigen::Vector3d v_0 {1.0, 0.0, 0.0};
   //Times in seconds
   double t_0 = 0.0; 
   
   
   double compute_C(double z) const;
   double compute_S(double z) const;


   double compute_x_newton(double t_diff) const;
   double compute_Tn(double x_n, double z_n) const;
   double compute_DtDx(double x_n) const;
 };
 }

#endif // KEPLERCALC_HPP
