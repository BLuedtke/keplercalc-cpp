/**
 * @file keplercalc.cpp
 * @author Bernhard Luedtke
 * @brief 
 * @version 0.1
 * @date 2022-01-27
 * 
 * @copyright Copyright 2022 Bernhard Luedtke

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */



#include "keplercalc.hpp"

#include <cstdint>

#include <cmath>
#include <exception>
#include <numbers>

using Eigen::Vector3d;
using namespace bluedtke;

using std::pow;
using std::abs;
using std::sqrt;
using std::sin;
using std::sinh;
using std::cos;
using std::cosh;


r0_v0 KeplerCalc::calculate_positon_and_speed(double t_now) {
   
   
   // Check that Semi Major Axis is not 0 -> this saves checking down the line.
   if(abs(semiMajorAxis) < 0.00000000000000001){
      std::terminate();
   }
   
   double t_diff = t_now - t_0;
   r_0_length = r_0.norm();
   
   // Now can actually do some calculations
   // Determine universal variable x through newton iteration
   auto x = compute_x_newton(t_diff);
   // Apply 4.4-7 to get z
   auto z = pow(x, 2.0) / semiMajorAxis;
   // Get C series based on z (TODO: check if drawing this out is indeed faster)
   auto c_ = compute_C(z);
   
   //Get f and g from applying equations (4.4-31) and (4.4-32)
   auto f = (1.0 - (pow(x, 2.0) / r_0_length) * c_);
	auto g = (pow(x, 2.0) * (r_0.dot(v_0) * inv_sqMU_const) * c_ + r_0_length * x * (1.0 - z * c_)) * inv_sqMU_const;
   
   //Now compute r by applying 4.4-18
   Vector3d r = r_0 * f + v_0 * g;
   
   //Get fD and gD from equations 4.4-35 and 4.4-36
   auto f_d = (sqMU_const / (r_0_length * r.norm())) * x * (z * compute_S(z) - 1.0);
   auto g_d = 1.0 - (pow(x, 2.0) / r.norm()) * c_;
   
   Vector3d v = r_0 * f_d + v_0 * g_d;
   
   //t_0 = t_now;
   r0_v0 ret = {{r[0],r[1],r[2]}, {v[0],v[1],v[2]}};
   
   
   return ret;
}

double KeplerCalc::compute_x_newton(double t_diff) const {
   //First guess for x, lets call it x_n
   double x_n = (sqMU_const * t_diff) / semiMajorAxis;
   
   //Apply 4.4-7 to get z_n
   double z_n = pow(x_n, 2.0) / semiMajorAxis;
   //get guess for t_n based on x_n and z_n
   double t_n = compute_Tn(x_n, z_n);
   
   //Newton iteration, see 4.4-15
   for(std::int32_t i = 0; i < 200; ++i){
      x_n += (t_diff - t_n) / compute_DtDx(x_n);
      z_n = pow(x_n, 2.0) / semiMajorAxis;
      t_n = ((r_0.dot(v_0) * inv_sqMU_const) * pow(x_n, 2.0) * compute_C(z_n)
               + (1.0 - (r_0_length / semiMajorAxis)) * pow(x_n, 3.0) * compute_S(z_n)
               + r_0_length * x_n) 
            * inv_sqMU_const;
      if(abs(t_diff - t_n) < 0.000000000000001){
         return x_n;
      }
   }
   return x_n;
}

double KeplerCalc::compute_C(double z) const {
   
   if(z > 0.0) [[likely]]{
      return (1.0 - cos(sqrt(z))) / z;
   } else if (z < 0.0) {
      return (1.0 - cosh(sqrt(-z))) / z;
   } else {
      // This is a form of stumpff's formulas which can be used if z is exactly zero to avoid div by zero.
      double cz = 0.0;
      for (unsigned int k = 0; k < 10; ++k) {
			double res = (pow((-z), static_cast<double>(k)) / factorial_ll(2 * k + 2));
			
			if (abs(res) != 0.0 && !std::isnan(cz + res)) {
				cz += res;
			} else {
				return cz;
			}
		}
   }
   return 0.0;
}

double KeplerCalc::compute_S(double z) const {
   if(z > 0.0) [[likely]] {
      //double sq_z = std::sqrt(z);
      return (sqrt(z) - sin(sqrt(z))) / sqrt(pow(z,3.0));
   } else if (z < 0.0){
      //double sq_z = std::sqrt(std::abs(z));
      return (sinh(sqrt(abs(z))) - sqrt(abs(z))) / sqrt(pow(abs(z),3.0));
   } else {
      double sz = 0.0;
      for (unsigned int k = 0; k < 9; k++) {
			double res = (pow((-z), static_cast<double>(k)) / factorial_ll(2 * k + 3));
			if (abs(res) != 0.0 && !std::isnan(sz+res)) {
				sz += res;
			} else {
				return sz;
			}
		}
   }
   return 0.0;
}

double KeplerCalc::compute_Tn(double x_n, double z_n) const {
   return (r_0.dot(v_0) * inv_sqMU_const) * pow(x_n, 2.0) * compute_C(z_n)
         +  (1.0 - (r_0_length / semiMajorAxis)) * pow(x_n, 3.0) * compute_S(z_n)
         +  r_0_length * x_n;
}

double KeplerCalc::compute_DtDx(double x_n) const {
   //Test if drawing these out is actually any faster.
   //double x_n_p2 = std::pow(x_n, 2.0);
   double z_n = pow(x_n, 2.0) / semiMajorAxis;
   double c_ = compute_C(z_n);
   
   return (pow(x_n, 2.0) * c_ 
            + (r_0.dot(v_0) * inv_sqMU_const) * x_n * (1.0 - z_n * compute_S(z_n))   
            + r_0_length * (1.0 - z_n * c_)) * inv_sqMU_const;
}

