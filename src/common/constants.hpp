/**
 * @file constants.hpp
 * @author Bernhard Luedtke
 * @brief 
 * @version 0.1
 * @date 2022-01-27
 * 
 * @copyright Copyright 2022 Bernhard Luedtke

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
 
 

#ifndef CONSTANTS_HPP
#define CONSTANTS_HPP

namespace bluedtke {
   constexpr double mu_const = 398600.4415; // km³/s²
   constexpr double sqMU_const = 631.34811435530557067107826305322534788801287186380085058022;
   constexpr double inv_sqMU_const = 0.0015839122304517205876478645672880554194971027694253089546069370; // 1.0 / sqMU_const;
   
   constexpr double mu_const_simple = 398600.0; //km³/s²
   
   constexpr long long factorial_ll(int n)
   {
      return (n == 1 || n == 0) ? 1 : factorial_ll(n - 1) * n;
   }
}

#endif // CONSTANTS_HPP
