/**
 * @file Matrix4.cpp
 * @author Bernhard Luedtke
 * @brief Basic custom implementation of a 4x4 matrix class.
 *        Initial structure/base adapted from code originally written by Prof. Dr.-Ing. Philipp Lensing (HS Osnabrück).
 * @version 0.1
 * @date 2022-01-27
 * 
 * @copyright Copyright 2022 Bernhard Luedtke

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
#include "Matrix4.hpp"

#include <cmath>
#include <numbers>
#include <assert.h>
#include <iostream>
#include <iomanip>

#define WEAK_EPSILON 1e-6

using namespace bluedtke;

using std::abs;

//Matrix Default constructor initialises the identity (matrix).
Matrix4::Matrix4()
{
    m00 = 1.0;	m01 = 0.0;	m02 = 0.0;	m03 = 0.0;
    m10 = 0.0;	m11 = 1.0;	m12 = 0.0;	m13 = 0.0;
    m20 = 0.0;	m21 = 0.0;	m22 = 1.0;	m23 = 0.0;
    m30 = 0.0;	m31 = 0.0;	m32 = 0.0;	m33 = 1.0;
}

Matrix4::Matrix4( double _00, double _01, double _02, double _03,
                double _10, double _11, double _12, double _13,
                double _20, double _21, double _22, double _23,
                double _30, double _31, double _32, double _33 ) :
    m00(_00), m01(_01), m02(_02), m03(_03),
    m10(_10), m11(_11), m12(_12), m13(_13),
    m20(_20), m21(_21), m22(_22), m23(_23),
    m30(_30), m31(_31), m32(_32), m33(_33)
{
}

Matrix4::operator double*()
{
    return m;
}
Matrix4::operator const double* const()
{
    return m;
}

Matrix4 Matrix4::operator*(const Matrix4& M) const
{
    Matrix4 Out = *this;
    Out.multiply(M);
    return Out;
}
Matrix4& Matrix4::operator*=(const Matrix4& M)
{
    multiply(M);
    return *this;
}

Vector Matrix4::operator*(const Vector& v) const
{
    return transformVec4x4(v);
}

bool Matrix4::operator==(const Matrix4& M)
{
    const double Epsilon = WEAK_EPSILON;
    return abs(m00-M.m00)<=Epsilon && abs(m01-M.m01)<=Epsilon && abs(m02-M.m02)<=Epsilon && abs(m03-M.m03)<=Epsilon &&
    abs(m10-M.m10)<=Epsilon && abs(m11-M.m11)<=Epsilon && abs(m12-M.m12)<=Epsilon && abs(m13-M.m13)<=Epsilon &&
    abs(m20-M.m20)<=Epsilon && abs(m21-M.m21)<=Epsilon && abs(m22-M.m22)<=Epsilon && abs(m23-M.m23)<=Epsilon &&
    abs(m30-M.m30)<=Epsilon && abs(m31-M.m31)<=Epsilon && abs(m32-M.m32)<=Epsilon && abs(m33-M.m33)<=Epsilon;
}

Vector Matrix4::transformVec4x4( const Vector& v) const
{
    double X = m00*v.X + m01*v.Y + m02*v.Z + m03;
    double Y = m10*v.X + m11*v.Y + m12*v.Z + m13;
    double Z = m20*v.X + m21*v.Y + m22*v.Z + m23;
    double W = m30*v.X + m31*v.Y + m32*v.Z + m33;
    return Vector( X/W, Y/W, Z/W);
}

Vector Matrix4::transformVec3x3( const Vector& v) const
{
    const double xT = m00*v.X + m01*v.Y + m02*v.Z;
    const double yT = m10*v.X + m11*v.Y + m12*v.Z;
    const double zT = m20*v.X + m21*v.Y + m22*v.Z;
    return Vector( xT, yT, zT);
}


bool Matrix4::operator!=(const Matrix4& M)
{
    return !(*this==M);
}

Vector Matrix4::left() const
{
    return Vector(-m00, -m10, -m20);
}

Vector Matrix4::right() const
{
    return Vector(m00, m10, m20);
}

Vector Matrix4::up() const
{
    return Vector(m01, m11, m21);
}

Vector Matrix4::down() const
{
    return Vector(-m01, -m11, -m21);
}

Vector Matrix4::forward() const
{
    return Vector(m02, m12, m22);
}

Vector Matrix4::backward() const
{
    return Vector(-m02, -m12, -m22);
}

Vector Matrix4::translation() const
{
    return Vector(m03, m13, m23);
}

void Matrix4::up(const Vector& v)
{
    m01 = v.X;
    m11 = v.Y;
    m21 = v.Z;
}

void Matrix4::forward(const Vector& v)
{
    m02 = v.X;
    m12 = v.Y;
    m22 = v.Z;
}

void Matrix4::right(const Vector& v)
{
    m00 = v.X;
    m10 = v.Y;
    m20 = v.Z;
}

Matrix4& Matrix4::multiply(const Matrix4& M )
{
    const Matrix4& A = *this;
    
    Matrix4 Tmp(
        A.m00 * M.m00 + A.m01 * M.m10  + A.m02 * M.m20 + A.m03 * M.m30,
        A.m00 * M.m01 + A.m01 * M.m11  + A.m02 * M.m21 + A.m03 * M.m31,
        A.m00 * M.m02 + A.m01 * M.m12  + A.m02 * M.m22 + A.m03 * M.m32,
        A.m00 * M.m03 + A.m01 * M.m13  + A.m02 * M.m23 + A.m03 * M.m33,

        A.m10 * M.m00 + A.m11 * M.m10  + A.m12 * M.m20 + A.m13 * M.m30,
        A.m10 * M.m01 + A.m11 * M.m11  + A.m12 * M.m21 + A.m13 * M.m31,
        A.m10 * M.m02 + A.m11 * M.m12  + A.m12 * M.m22 + A.m13 * M.m32,
        A.m10 * M.m03 + A.m11 * M.m13  + A.m12 * M.m23 + A.m13 * M.m33,

        A.m20 * M.m00 + A.m21 * M.m10  + A.m22 * M.m20 + A.m23 * M.m30,
        A.m20 * M.m01 + A.m21 * M.m11  + A.m22 * M.m21 + A.m23 * M.m31,
        A.m20 * M.m02 + A.m21 * M.m12  + A.m22 * M.m22 + A.m23 * M.m32,
        A.m20 * M.m03 + A.m21 * M.m13  + A.m22 * M.m23 + A.m23 * M.m33,

        A.m30 * M.m00 + A.m31 * M.m10  + A.m32 * M.m20 + A.m33 * M.m30,
        A.m30 * M.m01 + A.m31 * M.m11  + A.m32 * M.m21 + A.m33 * M.m31,
        A.m30 * M.m02 + A.m31 * M.m12  + A.m32 * M.m22 + A.m33 * M.m32,
        A.m30 * M.m03 + A.m31 * M.m13  + A.m32 * M.m23 + A.m33 * M.m33 );
    *this = Tmp;
    return *this;
}

Matrix4& Matrix4::translation(double X, double Y, double Z )
{
    m00= 1.0;	m01= 0.0;	m02= 0.0;	m03= X;
    m10= 0.0;	m11= 1.0;	m12= 0.0;	m13= Y;
    m20= 0.0;	m21= 0.0;	m22= 1.0;	m23= Z;
    m30= 0.0;	m31= 0.0;	m32= 0.0;	m33= 1.0;
    return *this;
}

Matrix4& Matrix4::translation(const Vector& XYZ )
{
    return translation(XYZ.X, XYZ.Y, XYZ.Z);
}
Matrix4& Matrix4::rotationX(double Angle )
{
    m00= 1.0;	m01= 0.0;	m02= 0.0;	m03= 0.0;
    m10= 0.0;					        m13= 0.0;
    m20= 0.0;					        m23= 0.0;
    m30= 0.0;	m31= 0.0;	m32= 0.0;	m33= 1.0;
    m11 = cos(Angle);
    m22 = m11;
    //SWAPPED
    m12 = sin(Angle);
    m21 = -m12;
    
    return *this;
}

Matrix4& Matrix4::rotationY(double Angle )
{
                m01= 0.0;               m03= 0.0;
    m10= 0.0;	m11= 1.0;	m12= 0.0;	m13= 0.0;
                m21= 0.0;               m23= 0.0;
    m30= 0.0;	m31= 0.0;	m32= 0.0;	m33= 1.0;
    
    m00 = m22 = cos(Angle);
    //SWAPPED
    m20 = sin(Angle);
    m02 = -m20;
    //m02 = sin(Angle);
    //m20 = -m02;
    
    return *this;
}

Matrix4& Matrix4::rotationZ(double Angle )
{
                            m02= 0.0;	m03= 0.0;
                            m12= 0.0;	m13= 0.0;
    m20= 0.0;	m21= 0.0;	m22= 1.0;	m23= 0.0;
    m30= 0.0;	m31= 0.0;	m32= 0.0;	m33= 1.0;
    
    m00 = m11 = cos(Angle);
    //SWAPPED
    m01= sin(Angle);
    m10= -m01;
    
    return *this;
}

Matrix4& Matrix4::rotationYawPitchRoll(double Yaw, double Pitch, double Roll )
{
    double cosx = cos(Pitch);
    double cosy = cos(Yaw);
    double cosz = cos(Roll);
    
    double sinx = sin(Pitch);
    double siny = sin(Yaw);
    double sinz = sin(Roll);
    
    m00 = (cosz*cosy + sinz*sinx*siny);
    m10 = (sinz*cosx);
    m20 = (-cosz*siny + sinz*sinx*cosy);
    m30 = 0.0;

    m01 = (-sinz*cosy + cosz*sinx*siny);
    m11 = (cosz*cosx);
    m21 = (sinz*siny + cosz*sinx*cosy);
    m31 = 0.0;
    
    m02 = (cosx*siny);
    m12 = (-sinx);
    m22 = (cosx*cosy);
    m32 = 0.0;
    
    m03 = m13 = m23 = 0.0;
    m33 = 1.0;
    
    return *this;
}

Matrix4& Matrix4::rotationYawPitchRoll(const Vector& Angles )
{
    rotationYawPitchRoll(Angles.X, Angles.Y, Angles.Z);
    return *this;
}

Matrix4& Matrix4::rotationAxis(const Vector& Axis, double Angle)
{
    
    const double Si = sin(Angle);
    const double Co = cos(Angle);
    const double OMCo = 1.0 - Co;
    Vector Ax = Axis;
    Ax.normalize();

    m00= (Ax.X * Ax.X) * OMCo + Co;
    m01= (Ax.X * Ax.Y) * OMCo - (Ax.Z * Si);
    m02= (Ax.X * Ax.Z) * OMCo + (Ax.Y * Si);
    m03= 0.0;
    
    m10= (Ax.Y * Ax.X) * OMCo + (Ax.Z * Si);
    m11= (Ax.Y * Ax.Y) * OMCo + Co;
    m12= (Ax.Y * Ax.Z) * OMCo - (Ax.X * Si);
    m13= 0.0;
    
    m20= (Ax.Z * Ax.X) * OMCo - (Ax.Y * Si);
    m21= (Ax.Z * Ax.Y) * OMCo + (Ax.X * Si);
    m22= (Ax.Z * Ax.Z) * OMCo + Co;
    m23= 0.0;
    
    m30= 0.0;
    m31= 0.0;
    m32= 0.0;
    m33= 1.0;
    
    return *this;
}
Matrix4& Matrix4::scale(double ScaleX, double ScaleY, double ScaleZ )
{
    m00= ScaleX;	m01= 0.0;		m02= 0.0;		m03= 0.0;
    m10= 0.0;   	m11= ScaleY;	m12= 0.0;		m13= 0.0;
    m20= 0.0;		m21= 0.0;		m22= ScaleZ;	m23= 0.0;
    m30= 0.0;		m31= 0.0;		m32= 0.0;		m33= 1.0;
    
    return *this;
}
Matrix4& Matrix4::scale(const Vector& Scalings )
{
    scale( Scalings.X, Scalings.Y, Scalings.Z);
    return *this;
}
Matrix4& Matrix4::scale(double Scaling )
{
    scale(Scaling, Scaling, Scaling);
    return *this;
}

Matrix4& Matrix4::identity()
{
    m00= 1.0;	m01= 0.0;	m02= 0.0;	m03= 0.0;
    m10= 0.0;	m11= 1.0;	m12= 0.0;	m13= 0.0;
    m20= 0.0;	m21= 0.0;	m22= 1.0;	m23= 0.0;
    m30= 0.0;	m31= 0.0;	m32= 0.0;	m33= 1.0;
    return *this;
}

Matrix4& Matrix4::transpose()
{
    Matrix4 Tmp(
      m00, m10, m20, m30,
      m01, m11, m21, m31,
      m02, m12, m22, m32,
      m03, m13, m23, m33 );
    *this = Tmp;
    return *this;
}

Matrix4& Matrix4::invert()
{
    double num5 = m00;
    double num4 = m01;
    double num3 = m02;
    double num2 = m03;
    double num9 = m10;
    double num8 = m11;
    double num7 = m12;
    double num6 = m13;
    double num17 = m20;
    double num16 = m21;
    double num15 = m22;
    double num14 = m23;
    double num13 = m30;
    double num12 = m31;
    double num11 = m32;
    double num10 = m33;
    double num23 = (num15 * num10) - (num14 * num11);
    double num22 = (num16 * num10) - (num14 * num12);
    double num21 = (num16 * num11) - (num15 * num12);
    double num20 = (num17 * num10) - (num14 * num13);
    double num19 = (num17 * num11) - (num15 * num13);
    double num18 = (num17 * num12) - (num16 * num13);
    double num39 = ((num8 * num23) - (num7 * num22)) + (num6 * num21);
    double num38 = -(((num9 * num23) - (num7 * num20)) + (num6 * num19));
    double num37 = ((num9 * num22) - (num8 * num20)) + (num6 * num18);
    double num36 = -(((num9 * num21) - (num8 * num19)) + (num7 * num18));
    double num = (1.0 / ((((num5 * num39) + (num4 * num38)) + (num3 * num37)) + (num2 * num36)));
    m00 = num39 * num;
    m10 = num38 * num;
    m20 = num37 * num;
    m30 = num36 * num;
    m01 = -(((num4 * num23) - (num3 * num22)) + (num2 * num21)) * num;
    m11 = (((num5 * num23) - (num3 * num20)) + (num2 * num19)) * num;
    m21 = -(((num5 * num22) - (num4 * num20)) + (num2 * num18)) * num;
    m31 = (((num5 * num21) - (num4 * num19)) + (num3 * num18)) * num;
    double num35 = (num7 * num10) - (num6 * num11);
    double num34 = (num8 * num10) - (num6 * num12);
    double num33 = (num8 * num11) - (num7 * num12);
    double num32 = (num9 * num10) - (num6 * num13);
    double num31 = (num9 * num11) - (num7 * num13);
    double num30 = (num9 * num12) - (num8 * num13);
    m02 = (((num4 * num35) - (num3 * num34)) + (num2 * num33)) * num;
    m12 = -(((num5 * num35) - (num3 * num32)) + (num2 * num31)) * num;
    m22 = (((num5 * num34) - (num4 * num32)) + (num2 * num30)) * num;
    m32 = -(((num5 * num33) - (num4 * num31)) + (num3 * num30)) * num;
    double num29 = (num7 * num14) - (num6 * num15);
    double num28 = (num8 * num14) - (num6 * num16);
    double num27 = (num8 * num15) - (num7 * num16);
    double num26 = (num9 * num14) - (num6 * num17);
    double num25 = (num9 * num15) - (num7 * num17);
    double num24 = (num9 * num16) - (num8 * num17);
    m03 = -(((num4 * num29) - (num3 * num28)) + (num2 * num27)) * num;
    m13 = (((num5 * num29) - (num3 * num26)) + (num2 * num25)) * num;
    m23 = -(((num5 * num28) - (num4 * num26)) + (num2 * num24)) * num;
    m33 = (((num5 * num27) - (num4 * num25)) + (num3 * num24)) * num;
    return *this;
}
Matrix4& Matrix4::lookAt(const Vector& Target, const Vector& Up, const Vector& Position )
{
    Vector f = Target-Position;
    f.normalize();
    Vector u = Up;
    u.normalize();
    Vector r = f.cross(u);
    r.normalize();
    u = r.cross(f);
    m00 = r.X;   m01 = r.Y;   m02 = r.Z;   m03 = -(r.dot(Position));
    m10 = u.X;   m11 = u.Y;   m12 = u.Z;   m13 = -(u.dot(Position));
    m20 = -f.X;  m21 = -f.Y;  m22 = -f.Z;  m23 = (f.dot(Position));
    m30 = 0.0;   m31 = 0.0;   m32 = 0.0;   m33 = 1.0;
    return *this;
}

Matrix4& Matrix4::perspective(double Fovy, double AspectRatio, double NearPlane, double FarPlane )
{
    assert(NearPlane < FarPlane);
    
    const double f = 1.0 / tan(Fovy * 0.5);
    const double NearMinusFar = NearPlane - FarPlane;
    
    m01 = m02 = m03 = 0.0;
    m10 = m12 = m13 = 0.0;
    m20 = m21 = 0.0;
    m30 = m31 = m33 = 0.0;
    m32 = -1.0;
    
    m00 = f / AspectRatio;
    m11 = f;
    m22 = (FarPlane+NearPlane) / NearMinusFar;
    m23 = 2.0 * FarPlane * NearPlane / NearMinusFar;
    return *this;
}

Matrix4& Matrix4::orthographic(double Width, double Height, double Near, double Far )
{
    double FMN = 1.0 / (Far-Near);
    m00 = 2.0/Width;   m01 = 0.0;         m02 = 0.0;      m03 = 0.0;
    m10 = 0.0;         m11 = 2.0/Height;  m12 = 0.0;      m13 = 0.0;
    m20 = 0.0;         m21 = 0.0;         m22 = -2.0*FMN; m23 = -(Far+Near)*FMN;
    m30 = 0.0;         m31 = 0.0;         m32 = 0.0;      m33 = 1.0;
    return *this;
}

double Matrix4::determinat()
{
    return	m00 * (m11 * m22 - m12 * m21) -
    m01 * (m10 * m22 - m12 * m20) +
    m02 * (m10 * m21 - m11 * m20);
}

void bluedtke::Matrix4::print() const {
    std::cout << "{\n";
    std::cout << std::setw(20) << m00 << "; " << std::setw(20) << m01 << "; " << std::setw(20) << m02 << "; " << std::setw(20) << m03 << ";\n";
    std::cout << std::setw(20) << m10 << "; " << std::setw(20) << m11 << "; " << std::setw(20) << m12 << "; " << std::setw(20) << m13 << ";\n";
    std::cout << std::setw(20) << m20 << "; " << std::setw(20) << m21 << "; " << std::setw(20) << m22 << "; " << std::setw(20) << m23 << ";\n";
    std::cout << std::setw(20) << m30 << "; " << std::setw(20) << m31 << "; " << std::setw(20) << m32 << "; " << std::setw(20) << m33 << ";\n";
    std::cout << "}\n";
}

