/**
 * @file Vector.cpp
 * @author Bernhard Luedtke
 * @brief Basic implementation of a vector holding three doubles.
 * @version 0.1
 * @date 2022-01-27
 * 
 * @copyright Copyright 2022 Bernhard Luedtke

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
#include "Vector.hpp"
#include <cmath>
using namespace bluedtke;

using std::abs;

Vector::Vector(double x, double y, double z)
{
	X = x;
	Y = y;
	Z = z;
}

Vector::Vector()
{
	X = 0.0;
	Y = 0.0;
	Z = 0.0;
}

double Vector::dot(const Vector& v) const
{
	return X * v.X + Y * v.Y + Z * v.Z;
}

Vector Vector::cross(const Vector& v) const
{
	double a = (Y*v.Z) - (Z*v.Y);
	double b = (Z*v.X) - (X*v.Z);
	double c = (X*v.Y) - (Y*v.X);
	return Vector(a, b, c);
}


Vector Vector::operator+(const Vector& v) const
{
	double a = X + v.X;
	double b = Y + v.Y;
	double c = Z + v.Z;
	return Vector(a, b, c);
}

Vector Vector::operator-(const Vector& v) const
{
	double a = X - v.X;
	double b = Y - v.Y;
	double c = Z - v.Z;
	return Vector(a, b, c);
}

Vector Vector::operator-() const
{
	return Vector(-X, -Y, -Z);
}



Vector& Vector::operator+=(const Vector& v)
{
	this->X = this->X + v.X;
	this->Y = this->Y + v.Y;
	this->Z = this->Z + v.Z;
	return *this;
}

Vector& Vector::normalize()
{
	auto kehrwert = 1.0 / this->length();
	X *= kehrwert;
	Y *= kehrwert;
	Z *= kehrwert;
	return  *this;
}

bool Vector::operator==(const Vector & v) const
{
	if (X == v.X && Y == v.Y && Z == v.Z) {
		return true;
	}
	return false;
}

double Vector::length() const
{
	return sqrt(this->lengthSquared());
}


double Vector::lengthSquared() const
{
	return this->dot(*this);
}

Vector Vector::reflection(const Vector& normal) const
{
	auto s = -2.0 * (this->dot(normal));
	return *this + normal * s;
}
/*
std::string Vector::toString() const
{
	return "X: " + std::to_string(this->X) + "; Y: " + std::to_string(this->Y) + "; Z: " + std::to_string(this->Z);
}

void Vector::print() const {
	std::cout << this->toString() << std::endl;
}*/

bool Vector::triangleIntersection(const Vector& d, const Vector& a, const Vector& b, const Vector& c, double& s) const
{
	Vector n = ((b - a).cross(c - a)).normalize();
	
	double ndL = n.dot(d);
	
	//Beam and plane in parallel?
	if (abs(ndL) <= 1e-20) {
		return false;
	}
	
	double dl = n.dot(a);
	s = (dl - n.dot(*this)) / ndL;
	
	if (s < 1e-20) {
		return false;
	}
	Vector p = *this + d * s;
	Vector pMa = p - a;
	return triangleArea(a, b, c) + 1e-5 >= (triangleAreaX(a, b, pMa) + triangleAreaX(a, c, pMa) + triangleArea(b, c, p));
	//return triangleArea(a,b,c) + 1e-5 >= (triangleArea(a,b,p) + triangleArea(a,c,p) + triangleArea(b,c,p));   
}


bool Vector::triangleIntersectionInformed(const Vector & d, const Vector & a, const Vector & b, const Vector & c, double & s, const Vector tN) const
{
	Vector n = tN;
	//Vector n = ((b - a).cross(c - a)).normalize();
	
	double ndL = n.dot(d);
	
	//Beam and plane in parallel?
	if (abs(ndL) <= 1e-20) {
		return false;
	}
	
	double dl = n.dot(a);
	s = (dl - n.dot(*this)) / ndL;
	
	if (s < 1e-20) {
		return false;
	}
	
	Vector p = *this + d * s;
	Vector pMa = p - a;
	return triangleArea(a, b, c) + 1e-5 >= (triangleAreaX(a, b, pMa) + triangleAreaX(a, c, pMa) + triangleArea(b, c, p));
}


double Vector::triangleAreaX(const Vector & u, const Vector & v, const Vector & wMu) const
{
	return 0.5 * ((v - u).cross(wMu)).length();
}

double Vector::triangleArea(const Vector& u, const Vector& v, const Vector& w) const
{
	return 0.5 * ((v - u).cross(w - u)).length();
}
