/**
 * @file Vector.hpp
 * @author Bernhard Luedtke
 * @brief Basic implementation of a vector holding three doubles.
 * @version 0.1
 * @date 2022-01-27
 * 
 * @copyright Copyright 2022 Bernhard Luedtke

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
#ifndef Vector_hpp
#define Vector_hpp


namespace bluedtke {
class Vector
{
public:
	double X;
	double Y;
	double Z;

	Vector(double x, double y, double z);
	Vector();
	Vector(Vector&&) noexcept = default;
	Vector(const Vector&) noexcept = default;

	double dot(const Vector& v) const;
	Vector cross(const Vector& v) const;
	Vector& normalize();

	Vector operator+(const Vector& v) const;
	Vector operator-(const Vector& v) const;
	Vector& operator+=(const Vector& v);
	
	friend Vector operator* (const Vector& v, double c){
		double a = v.X * c;
		double b = v.Y * c;
		double d = v.Z * c;
		return Vector(a, b, d);
	};
	friend Vector operator* (double c, const Vector& v){
		double a = v.X * c;
		double b = v.Y * c;
		double d = v.Z * c;
		return Vector(a, b, d);
	};
	
	Vector operator-() const;
	Vector& operator=(Vector&&) noexcept = default;
	Vector& operator=(const Vector&) noexcept = default;
	bool operator==(const Vector& v) const;

	double length() const;
	double lengthSquared() const;
	
	Vector reflection(const Vector& normal) const;
	
	bool triangleIntersection(const Vector& d, const Vector& a, const Vector& b, const Vector& c, double& s) const;
	bool triangleIntersectionInformed(const Vector& d, const Vector& a, const Vector& b, const Vector& c, double& s, const Vector tN) const;
	double triangleArea(const Vector& u, const Vector& v, const Vector& w) const;
	double triangleAreaX(const Vector& u, const Vector& v, const Vector& wMu) const;
};
}

#endif /* Vector_hpp */
