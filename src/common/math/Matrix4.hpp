/**
 * @file Matrix4.hpp
 * @author Bernhard Luedtke
 * @brief Basic custom implementation of a 4x4 matrix class.
 *        Initial structure/base adapted from code originally written by Prof. Dr.-Ing. Philipp Lensing (HS Osnabrück).
 * @version 0.1
 * @date 2022-01-27
 * 
 * @copyright Copyright 2022 Bernhard Luedtke

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
#ifndef Matrix_hpp
#define Matrix_hpp

#include "Vector.hpp"
namespace bluedtke {
class Matrix4
{
public:
    union
    {
        struct {
            double m00, m10,m20,m30;
            double m01, m11,m21,m31;
            double m02, m12,m22,m32;
            double m03, m13,m23,m33;
        };
        struct { double m[16]; }; //?????
    };
    Matrix4();
    Matrix4( double _00, double _01, double _02, double _03,
            double _10, double _11, double _12, double _13,
            double _20, double _21, double _22, double _23,
            double _30, double _31, double _32, double _33 );

    Matrix4(Matrix4&&) noexcept = default;
    Matrix4(const Matrix4 &) noexcept = default;
    
    operator double*();
    operator const double* const();
    
    Matrix4 operator*(const Matrix4& M) const;
    Matrix4& operator*=(const Matrix4& M);
    Vector operator*(const Vector& v) const;
    Matrix4& operator=(Matrix4&&) noexcept = default;
    Matrix4& operator=(const Matrix4&) noexcept = default;
    
    bool operator==(const Matrix4& M);
    bool operator!=(const Matrix4& M);
    
    void print() const;

    Vector left() const;
    Vector right() const;
    Vector up() const;
    Vector down() const;
    Vector forward() const;
    Vector backward() const;
    Vector translation() const;
    
    void up(const Vector& v);
    void forward(const Vector& v);
    void right(const Vector& v);
    
    Matrix4& multiply(const Matrix4& M);
    Matrix4& translation(double X, double Y, double Z);
    Matrix4& translation(const Vector& XYZ );
    Matrix4& rotationX(double Angle );
    Matrix4& rotationY(double Angle );
    Matrix4& rotationZ(double Angle );
    Matrix4& rotationYawPitchRoll(double Yaw, double Pitch, double Roll );
    Matrix4& rotationYawPitchRoll(const Vector& Angles);
    Matrix4& rotationAxis(const Vector& Axis, double Angle);
    Matrix4& scale(double ScaleX, double ScaleY, double ScaleZ);
    Matrix4& scale(const Vector& Scalings);
    Matrix4& scale(double Scaling);
    Matrix4& identity();
    Matrix4& transpose();
    Matrix4& invert();
    Matrix4& lookAt(const Vector& Target, const Vector& Up, const Vector& Position);
    Matrix4& perspective(double Fovy, double AspectRatio, double NearPlane, double FarPlane);
    Matrix4& orthographic(double Width, double Height, double Near, double Far);
    Vector transformVec4x4(const Vector& v) const;
    Vector transformVec3x3(const Vector& v) const;
    double determinat();
};
}

#endif /* Matrix_hpp */
