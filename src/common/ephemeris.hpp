/**
 * @file ephemeris.hpp
 * @author Bernhard Luedtke
 * @brief 
 * @version 0.1
 * @date 2022-01-27
 * 
 * @copyright Copyright 2022 Bernhard Luedtke

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

#ifndef EPHEMERIS_HPP
#define EPHEMERIS_HPP

#include "constants.hpp"
#include <cmath>
#include <tuple>
#include <numbers>
#include <iostream>
#include "../include/vallado/MathTimeLib.hpp"
#include "math/Matrix4.hpp"
#include "math/Vector.hpp"

namespace bluedtke {
   
   constexpr double smallThreshold = 1e-7;
   constexpr double smallerThreshold = 1e-8;
   
   constexpr double degToRad(double angle){
      return angle * (std::numbers::pi / 180.0);
   }
   // See page 58 in [1] 
   struct orb_elements {
      double semiMajA;	   // a        | semi major axis
      double ecc;          // e        | eccentricity
      double incl;         // i        | inclination
      double longAsc;      // Omega    | longitude of the ascending node (rad); -> https://en.wikipedia.org/wiki/Longitude_of_the_ascending_node
      double argPer;	      // w (small Omega)   | argument of periapsis
      double nu;     // _v_ (italic v)    | True anomaly at epoch
      double T;            // T        | Time of periapsis passage -> technically replaced by trueAnomaly here (pg 60 [1])
   };
   
   struct orb_elements_ext {
     orb_elements orb;
     double argLat;        // argument of latitude (ci)
     double lamTrue;       // true longitude (ce)
     double longPer;       // longitude of periapsis (ee)
   };
   
   struct r0_v0{
      double r0[3];
      double v0[3];
   };
   
   struct v3{
      double X;
      double Y;
      double Z;
   };
   
   typedef struct orb_elements orb_elements;
   typedef struct orb_elements_ext orb_elements_ext;
   typedef struct r0_v0 r0_v0;
   typedef struct v3 v3;
   
   
   inline orb_elements_ext calculateExtendedEphemeris(const orb_elements& baseEphemeris){
      using namespace std;
      using std::numbers::pi;
      //shorthand
      orb_elements_ext orbE;
      orbE.orb = baseEphemeris;
      orbE.argLat = orbE.orb.argPer + orbE.orb.nu;
      orbE.lamTrue = orbE.orb.longAsc + orbE.argLat;
      orbE.longPer = orbE.orb.longAsc + orbE.orb.argPer;
      while (orbE.argLat > 2.0 * pi) {
         orbE.argLat = orbE.argLat - (2.0 * pi);
      }
      while (orbE.lamTrue > 2.0 * pi) {
         orbE.lamTrue = orbE.lamTrue - (2.0 * pi);
      }
      while (orbE.longPer > 2.0 * pi) {
         orbE.longPer = orbE.longPer - (2.0 * pi);
      }
      return orbE;
   }
   
   
   //inline v3 rot1(const v3& vec, double xVal){
   inline v3 rot1(v3 vec, double xVal) noexcept {
      using namespace std;
      auto c = cos(xVal);
      auto s = sin(xVal);
      v3 ret;
      ret.Z = c * vec.Z - s * vec.Y;
      ret.Y = c * vec.Y + s * vec.Z;
      ret.X = vec.X;
      return ret;
   }
   
   //inline v3 rot3(const v3& vec, double xVal) {
   inline v3 rot3(v3 vec, double xVal) noexcept {
      using namespace std;
      
      auto c = cos(xVal);
      auto s = sin(xVal);
      v3 ret;
      ret.Y = c * vec.Y - s * vec.X;
      ret.X = c * vec.X + s * vec.Y;
      ret.Z = vec.Z;
      return ret;
   }
   
   //BASIC VARIANT -> shall be compared to variants implemented using different math libs.
   // Info: See page 72ff in [1]
   // Adapted from code from David Vallado [2] (AstroLib.cpp starting from line 3675)
   inline r0_v0 get_r0v0_from_elements(const orb_elements_ext& orbital_elements){
      // calculating r and v in the perifocal system, then transfering to IJK.
      
      using namespace std;
      using std::numbers::pi;
      //shorthand
      orb_elements_ext orbE = orbital_elements;
      
      // Adjustments based on type of orbit
      if(orbE.orb.ecc < smallThreshold){
         if((orbE.orb.incl < smallThreshold) | ((abs(orbE.orb.incl - pi)) < smallThreshold)){
            //Circular Equatorial
            orbE.orb.argPer = 0.0;
            orbE.orb.longAsc = 0.0;
            orbE.orb.nu = orbE.lamTrue;
         } else {
            //Circular inclined
            orbE.orb.argPer = 0.0;
            orbE.orb.nu = orbE.argLat;
         }
      } else if((orbE.orb.incl < smallThreshold) | (abs(orbE.orb.incl - pi) < smallThreshold)){
         //Elliptical Equatorial
         orbE.orb.argPer = orbE.longPer;
         orbE.orb.longAsc = 0.0;
      }
      
      // Form PQW position and velocity vectors
      auto cos_nu = cos(orbE.orb.nu);
      auto sin_nu = sin(orbE.orb.nu);
      double p = orbE.orb.semiMajA * ( 1.0 - pow(orbE.orb.ecc, 2.0));
      double tmp = p / (1.0 + orbE.orb.ecc * cos_nu);
      if (abs(p) < smallerThreshold){
         p = smallerThreshold;
      }
      
      //Transform to IJK
      v3 rpqw {tmp * cos_nu, tmp * sin_nu, 0.0};
      v3 vpqw {-sin_nu * sqrt(mu_const / p), (orbE.orb.ecc + cos_nu) * sqrt(mu_const / p), 0.0};
      
      auto tmpVec = rot3(rpqw, -orbE.orb.argPer);
      tmpVec = rot1(tmpVec, -orbE.orb.incl);
      tmpVec = rot3(tmpVec, -orbE.orb.longAsc);
      auto tmpVec2 = rot3(vpqw, -orbE.orb.argPer);
      tmpVec2 = rot1(tmpVec2, -orbE.orb.incl);
      tmpVec2 = rot3(tmpVec2, -orbE.orb.longAsc);
      
      return { {tmpVec.X,tmpVec.Y,tmpVec.Z},{tmpVec2.X,tmpVec2.Y,tmpVec2.Z}};
   }
   
   inline r0_v0 get_r0v0_from_elements_tests(const orb_elements_ext& orbital_elements){
      // calculating r and v in the perifocal system, then transfering to IJK.
      
      using namespace std;
      using std::numbers::pi;
      //shorthand
      orb_elements_ext orbE = orbital_elements;
      
      // Adjustments based on type of orbit
      if(orbE.orb.ecc < smallThreshold){
         if((orbE.orb.incl < smallThreshold) | ((abs(orbE.orb.incl - pi)) < smallThreshold)){
            //Circular Equatorial
            orbE.orb.argPer = 0.0;
            orbE.orb.longAsc = 0.0;
            orbE.orb.nu = orbE.lamTrue;
         } else {
            //Circular inclined
            orbE.orb.argPer = 0.0;
            orbE.orb.nu = orbE.argLat;
         }
      } else if((orbE.orb.incl < smallThreshold) | (abs(orbE.orb.incl - pi) < smallThreshold)){
         //Elliptical Equatorial
         orbE.orb.argPer = orbE.longPer;
         orbE.orb.longAsc = 0.0;
      }
      
      // Form PQW position and velocity vectors
      auto cos_nu = cos(orbE.orb.nu);
      auto sin_nu = sin(orbE.orb.nu);
      double p = orbE.orb.semiMajA * ( 1.0 - pow(orbE.orb.ecc, 2.0));
      double tmp = p / (1.0 + orbE.orb.ecc * cos_nu);
      if (abs(p) < smallerThreshold){
         p = smallerThreshold;
      }
      
      v3 rpqw {tmp * cos_nu, tmp * sin_nu, 0.0};
      v3 vpqw {-sin_nu * sqrt(mu_const / p), (orbE.orb.ecc + cos_nu) * sqrt(mu_const / p), 0.0};
      
      //Transform to IJK
      
      auto tmpVec = rot3(rpqw, -orbE.orb.argPer);
      tmpVec = rot1(tmpVec, -orbE.orb.incl);
      tmpVec = rot3(tmpVec, -orbE.orb.longAsc);
      auto tmpVec2 = rot3(vpqw, -orbE.orb.argPer);
      tmpVec2 = rot1(tmpVec2, -orbE.orb.incl);
      tmpVec2 = rot3(tmpVec2, -orbE.orb.longAsc);
      
      return { {tmpVec.X,tmpVec.Y,tmpVec.Z},{tmpVec2.X,tmpVec2.Y,tmpVec2.Z}};
   }
   
   inline r0_v0 get_r0v0_from_elements_shortEph(const orb_elements& baseEphemeris){
      //orb_elements_ext orbE = calculateExtendedEphemeris(baseEphemeris);
      //return get_r0v0_from_elements(orbE);
      return get_r0v0_from_elements(calculateExtendedEphemeris(baseEphemeris));
   }
   
   
   //Taken directly from Vallado [2]:
   inline void coe2rv
	(
		double p, double ecc, double incl, double raan, double argp, double nu,
		double arglat, double truelon, double lonper,
		double r[3], double v[3]
	)
	{
		double rpqw[3], vpqw[3], tempvec[3], temp, sinnu, cosnu, small;
      using namespace std;
      using std::numbers::pi;
		small = 0.0000001;

		// --------------------  implementation   ----------------------
		//       determine what type of orbit is involved and set up the
		//       set up angles for the special cases.
		// -------------------------------------------------------------
		if (ecc < small)
		{
			// ----------------  circular equatorial  ------------------
			if ((incl < small) | (abs(incl - pi) < small))
			{
				argp = 0.0;
				raan = 0.0;
				nu = truelon;
			}
			else
			{
				// --------------  circular inclined  ------------------
				argp = 0.0;
				nu = arglat;
			}
		}
		else
		{
			// ---------------  elliptical equatorial  -----------------
			if ((incl < small) | (abs(incl - pi) < small))
			{
				argp = lonper;
				raan = 0.0;
			}
		}

		// ----------  form pqw position and velocity vectors ----------
		cosnu = cos(nu);
		sinnu = sin(nu);
		temp = p / (1.0 + ecc * cosnu);
		rpqw[0] = temp * cosnu;
		rpqw[1] = temp * sinnu;
		rpqw[2] = 0.0;
		if (abs(p) < 0.00000001)
			p = 0.00000001;

		vpqw[0] = -sinnu * sqrt(mu_const / p);
		vpqw[1] = (ecc + cosnu) * sqrt(mu_const / p);
		vpqw[2] = 0.0;

		// ----------------  perform transformation to ijk  ------------
		MathTimeLib::rot3(rpqw, -argp, tempvec);
		MathTimeLib::rot1(tempvec, -incl, tempvec);
		MathTimeLib::rot3(tempvec, -raan, r);

		MathTimeLib::rot3(vpqw, -argp, tempvec);
		MathTimeLib::rot1(tempvec, -incl, tempvec);
		MathTimeLib::rot3(tempvec, -raan, v);
	}  // coe2rv
   
   //Taken directly from Vallado [2]:
   inline void coe2rv_no_p
	(
		double a, double ecc, double incl, double raan, double argp, double nu,
		double arglat, double truelon, double lonper,
		double r[3], double v[3]
	)
	{
      using namespace std;
      using std::numbers::pi;
		double rpqw[3], vpqw[3], tempvec[3], temp, sinnu, cosnu, small, p;
      p = a * ( 1.0 - pow(ecc, 2.0));
		small = 0.0000001;

		// --------------------  implementation   ----------------------
		//       determine what type of orbit is involved and set up the
		//       set up angles for the special cases.
		// -------------------------------------------------------------
		if (ecc < small)
		{
			// ----------------  circular equatorial  ------------------
			if ((incl < small) | (abs(incl - pi) < small))
			{
				argp = 0.0;
				raan = 0.0;
				nu = truelon;
			}
			else
			{
				// --------------  circular inclined  ------------------
				argp = 0.0;
				nu = arglat;
			}
		}
		else
		{
			// ---------------  elliptical equatorial  -----------------
			if ((incl < small) | (abs(incl - pi) < small))
			{
				argp = lonper;
				raan = 0.0;
			}
		}

		// ----------  form pqw position and velocity vectors ----------
		cosnu = cos(nu);
		sinnu = sin(nu);
		temp = p / (1.0 + ecc * cosnu);
		rpqw[0] = temp * cosnu;
		rpqw[1] = temp * sinnu;
		rpqw[2] = 0.0;
		if (abs(p) < 0.00000001)
			p = 0.00000001;

		vpqw[0] = -sinnu * sqrt(mu_const / p);
		vpqw[1] = (ecc + cosnu) * sqrt(mu_const / p);
		vpqw[2] = 0.0;

		// ----------------  perform transformation to ijk  ------------
		MathTimeLib::rot3(rpqw, -argp, tempvec);
		MathTimeLib::rot1(tempvec, -incl, tempvec);
		MathTimeLib::rot3(tempvec, -raan, r);

		MathTimeLib::rot3(vpqw, -argp, tempvec);
		MathTimeLib::rot1(tempvec, -incl, tempvec);
		MathTimeLib::rot3(tempvec, -raan, v);
	}  // coe2rv
   
   
   
   inline r0_v0 callVallado_coe2rv(const orb_elements& orbs){
      orb_elements_ext orbE = calculateExtendedEphemeris(orbs);
      
      double p = orbE.orb.semiMajA * ( 1.0 - pow(orbE.orb.ecc, 2.0));
      double r[3] {0.0,0.0,0.0};
      double v[3] {0.0,0.0,0.0};
      
      coe2rv(p, orbE.orb.ecc, orbE.orb.incl, orbE.orb.longAsc,
            orbE.orb.argPer, orbE.orb.nu, orbE.argLat, orbE.lamTrue, orbE.longPer, r, v);
      return { {r[0],r[1],r[2]},{v[0],v[1],v[2]}};
   }
   
   inline r0_v0 callVallado_coe2rv_ext(const orb_elements_ext& orbE){
      double p = orbE.orb.semiMajA * ( 1.0 - pow(orbE.orb.ecc, 2.0));
      double r[3] {0.0,0.0,0.0};
      double v[3] {0.0,0.0,0.0};
      
      coe2rv(p, orbE.orb.ecc, orbE.orb.incl, orbE.orb.longAsc,
            orbE.orb.argPer, orbE.orb.nu, orbE.argLat, orbE.lamTrue, orbE.longPer, r, v);
      return { {r[0],r[1],r[2]},{v[0],v[1],v[2]}};
   }
   
   inline void callVallado_coe2rv_ext_no_ret(const orb_elements_ext& orbE){
      double r[3] {0.0,0.0,0.0};
      double v[3] {0.0,0.0,0.0};
      //double p = orbE.orb.semiMajA * ( 1.0 - pow(orbE.orb.ecc, 2.0));
      
      coe2rv_no_p(orbE.orb.semiMajA, orbE.orb.ecc, orbE.orb.incl, orbE.orb.longAsc,
            orbE.orb.argPer, orbE.orb.nu, orbE.argLat, orbE.lamTrue, orbE.longPer, r, v);
   }
   
   
   //This _can_ run into problems for very circular and/or equatorial (incl = 0) orbits!
   inline r0_v0 get_r0v0_from_elements_legacy (const orb_elements_ext& orbital_elements){
      
      // Calculate PQW matrix
      /*
      auto _a = Matrix4().rotationY(orbital_elements.orb.longAsc);
      
      auto _b = Matrix4().rotationX(orbital_elements.orb.incl);
		
      auto _c = Matrix4().rotationY(orbital_elements.orb.argPer);
      
		auto pqw = Matrix4();
		
		pqw = pqw  * _a * _b * _c;
      */
      auto pqw = Matrix4();
		
		pqw = pqw  
               * (Matrix4().rotationY(orbital_elements.orb.longAsc)) 
               * (Matrix4().rotationX(orbital_elements.orb.incl)) 
               * (Matrix4().rotationY(orbital_elements.orb.argPer));
      
      Vector P = pqw.right();
      Vector Q = pqw.forward();
      
      
      double cosnu = cos(orbital_elements.orb.nu);
      double sinnu = sin(orbital_elements.orb.nu);
      
      double semLatRec = orbital_elements.orb.semiMajA * (1.0 - pow(orbital_elements.orb.ecc, 2.0));
      double rScale = semLatRec / (1.0 + orbital_elements.orb.ecc * cosnu);
      
      Vector r0 = rScale * cosnu * P + rScale * sinnu * Q;
      
      Vector temp = Q * (orbital_elements.orb.ecc + 1.0);
      Vector v0 = temp * sqrt(mu_const / semLatRec) * -1.0;
      
      Vector v0b = sqrt(mu_const / semLatRec) * (-sinnu * P + (orbital_elements.orb.ecc + cosnu) * Q);
      // Due to whatever reasons, I have to switch Z and Y in both vectors here.
      return {{r0.X, r0.Z, r0.Y},{v0b.X, v0b.Z, v0b.Y}};
   }
   
   
   
   
   
   
   
}
 
#endif // EPHEMERIS_HPP


/*
//NOTES
   // h^2 = mu * a * (1 - e^2)
   // p = h^2 / mu
   // p = a * (1 - e^2)
   
   // Eccentric anomaly E = (e + cos(_v_)) / (1 + e * cos(_v_))
   // Mean anomaly M = E - e * sin(E)
   
   // cos(_v_) = (cos(E) - e) / (1 - e * cos(E))
   
   // →r -> vector
   // r  -> scalar (r = p / (1 + e * cos(_v_))) (or just length of →r)
   // →r = r * cos(_v_) * P + r * sin(_v_) * Q
   // →v = sqrt(mu/p) * ( -sin(_v_)*P + (e+cos(_v_)*Q) )
   
   // Not sure:
   // _v0_ = cosh((e * →r)/(e * r))
*/
//-


// [1]: Bate, Mueller, White et al. "Fundamentals of Astrodynamics"
// [2]: David Vallado "Fundamentals of Astrodynamics and Applications" + Accompanying software